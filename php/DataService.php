<?php
	if($_GET) {
		header('content-type: text/javascript; charset=utf-8');
		$lbl = $_GET['label'];
		$chapter = $_GET['chapter'];
		echo $_GET['callback'].'('. fetchData($lbl, $chapter) .')';
	}

function fetchData($label, $chapter) {
	$uri = 'http://www.esvapi.org/v2/rest/passageQuery?key=IP&passage=';
	$uri .= str_replace(" ", '',$label);
	$uri .= '+'.$chapter;
	$uri .= '&output-format=plain-text';
	$uri .= '&include-passage-references=false';
	$uri .= '&include-footnotes=false';
	$uri .= '&include-short-copyright=false';
	$uri .= '&include-copyright=false';
	$uri .= '&include-passage-horizontal-lines=false';
	$uri .= '&include-heading-horizontal-lines=false';
	$uri .= '&include-headings=false';
	$uri .= '&include-subheadings=false';
	$uri .= '&include-content-type=false';
	$ch = curl_init($uri);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = new StdClass();
	$output = preg_replace('!\s+!', ' ', str_replace(']', '] ',str_replace("- ", ' ', str_replace("-", ' ', str_replace("\n", '',curl_exec($ch))))));
	$response->blob = $output;
	$response->output = explode(' ', trim($output));

	$length = count($response->output);

	for ($i = 0; $i < $length; $i++) {
		$word = $response->output[$i];
		$wordLen = strlen($word);
		$isEven = $wordLen % 2 == 0;
		$center = round($wordLen / 2) - 1;
		$s = $center - 1;
		$e = $wordLen - $center;
		$front = ''; 
		$back = ''; 
		$w = new StdClass();
		$verse = preg_match_all("/\[.*?\]/", $word, $match);

		if($verse) {
			$w->verse = str_replace("[", '',str_replace("]", '',implode($match[0])));
		} else {
			if($isEven) {
				$w->focus = substr($word, -($center + 2),  1);
			} else {
				$w->focus = substr($word, -($s + 2),  1);
			}
			$w->start = substr($word, 0,  $center - $wordLen);
			$w->back = substr($word, $center + 1,  $center + 1);
			$w->back = $w->back === false ? '' : $w->back; // for single char words
		}

		$response->output[$i] = $w;
	}

	$response->book = $label;
	$response->chapter = $chapter;

	return json_encode($response);
}