function MenuButton() {
	
	var self = document.createElement('div'),
		icon = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
		arrow = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
		label = document.createElement('div');

	MESSENGER.listen('serviceResponse', updateLabel);
	MESSENGER.listen('updateVerse', updateLabel);
	MESSENGER.listen("menuOpen", rotateIconDown);
	MESSENGER.listen("menuClose", rotateIconUp);

	label.addEventListener('mouseover', over);
	self.addEventListener('click', click);

	self.setAttribute('class', 'MenuButton');

	arrow.setAttribute('d', 'M 10,0 C 13.333333,3.3333333 16.666667,6.6666667 20,10 13.333333,10 6.6666667,10 0,10 3.3333333,6.6666667 6.6666667,3.3333333 10,0 z');
	arrow.setAttribute('style', 'fill:' + theme.fontLight + ';transition:fill 0.35s');

	icon.style.display = 'block';
	icon.style.position = 'absolute';
	icon.style.left = (window.innerWidth - 96 - 20) * 0.5 + 'px';
	icon.style.top = 10 + 'px';
	icon.style.width = 20 + 'px';
	icon.style.height = 10 + 'px';

	label.style.display = 'block';
	label.style.position = 'absolute';
	label.style.left = 0 + 'px';
	label.style.top = 24 + 'px';
	label.style.width = window.innerWidth - 96 + 'px';
	label.style.color = theme.fontLight;
	label.style.textAlign = 'center';
	label.style.fontFamily = 'Droid Sans Mono';
	label.style.fontSize = Theme.fontSize.medium;

	self.style.display = 'block';
	self.style.position = 'absolute';
	self.style.width = window.innerWidth - 96 + 'px';
	self.style.height = 60 + 'px';

	icon.appendChild(arrow);

	self.appendChild(icon);
	self.appendChild(label);

	function updateLabel(e) {
		curBook = e.book || curBook;
		curChapter = e.chapter || curChapter;
		curVerse = e.verse || curVerse;
		if(curVerse.length > 1) curVerse = 1;
		label.innerHTML = curBook + ' ' + '<span style="color:' + theme.highlight + '" >' + curChapter + ':' + curVerse + '</span>';
	}

	function over() {
		arrow.style.fill = theme.highlight;
		label.addEventListener(EVENT_OUT, out);
	}

	function out() {
		arrow.style.fill = theme.fontLight;
		label.addEventListener(EVENT_OUT, out);
	}

	function click() {
		MESSENGER.notify("MenuButton/click");
	}

	function rotateIconDown() {
		TweenMax.to(icon, 0.35, {rotation:180});
	}

	function rotateIconUp() {
		TweenMax.to(icon, 0.35, {rotation:0});
	}

	label.innerHTML = 'Menu';

	return self;
}