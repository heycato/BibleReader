function ToolBar() {
	var self = document.createElement('div'),
		reverse = new Control('reverse'),
		menuButton = new MenuButton(),
		forward = new Control('forward');


	MESSENGER.listen('forward/down', toolbarHide);
	MESSENGER.listen('forward/up', toolbarShow);

	self.setAttribute('class', 'ToolBar');
	self.style.display = 'block';
	self.style.position = 'absolute';
	self.style.backgroundColor = theme.bgDark;
	self.style.width = window.innerWidth + 'px';
	self.style.height = 60 + 'px';
	self.style.left = 0 + 'px';
	self.style.top = window.innerHeight - 60 + 'px';

	reverse.style.left = 20 + 'px';
	reverse.style.top = 17 + 'px';

	menuButton.style.left = (window.innerWidth - parseInt(menuButton.style.width, 10)) * 0.5 + 'px';
	menuButton.style.top = 0 + 'px';

	forward.style.left = window.innerWidth - 48 + 'px';
	forward.style.top = 17 + 'px';

	self.appendChild(reverse);
	self.appendChild(menuButton);
	self.appendChild(forward);

	self.resize = function() {
		TweenMax.to(self, 0.35, {width:window.innerWidth + 'px', top:window.innerHeight - 60 + 'px'});
		TweenMax.to(menuButton, 0.35, {left:(window.innerWidth - parseInt(menuButton.style.width, 10)) * 0.5 + 'px'});
		TweenMax.to(forward, 0.35, {left:window.innerWidth - 48 + 'px'});
	};

	function toolbarHide() {
		TweenMax.to(self, 0.35, {opacity:0.3});
	}

	function toolbarShow() {
		TweenMax.to(self, 0.35, {opacity:1});
	}

	return self;
}