function TextDisplay() {
	var self = this,
		start = document.getElementById('start'),
		focus = document.getElementById('focus'),
		end = document.getElementById('end'),
		cx = (window.innerWidth - focus.offsetWidth) * 0.5,
		cy = (window.innerHeight - focus.offsetHeight) * 0.5,
		timer,
		length,
		isPlaying = false,
		userDelay = 120, // 500wpm
		delay;

	MESSENGER.listen('serviceResponse', update);
	MESSENGER.listen('forward/down', playStart);
	MESSENGER.listen('reverse/down', rewindStart);
	MESSENGER.listen('reverse/up', stop);
	MESSENGER.listen('forward/up', stop);
	MESSENGER.listen('sliderUpdate', delayUpdate);

	focus.style.display = 'block';
	focus.style.position = 'absolute';
	focus.style.left = cx + 'px';
	focus.style.top = cy + 'px';
	focus.style.color = theme.highlight;
	focus.style.textDecoration = 'underline';
	//focus.style.backgroundColor = '#333333';
	focus.style.fontSize = theme.fontSize.large;

	start.style.display = 'block';
	start.style.position = 'absolute';
	start.style.left = cx - start.offsetWidth + 'px';
	start.style.top = cy + 'px';
	start.style.color = '#FFFFFF';
	start.style.fontSize = theme.fontSize.large;

	end.style.display = 'block';
	end.style.position = 'absolute';
	end.style.left = cx + focus.offsetWidth + 'px';
	end.style.top = cy + 'px';
	end.style.color = '#FFFFFF';
	end.style.fontSize = theme.fontSize.large;

	self.resize = function(noTween) {
		cx = (window.innerWidth - focus.offsetWidth) * 0.5;
		cy = (window.innerHeight - focus.offsetHeight) * 0.5;

		if(noTween) {
			start.style.left = cx - start.offsetWidth + 'px';
			start.style.top = cy + 'px';

			focus.style.left = cx + 'px';
			focus.style.top = cy + 'px';

			end.style.left = cx + focus.offsetWidth + 'px';
			end.style.top = cy + 'px';
		} else {
			TweenMax.to(start, 0.35, {left: cx - start.offsetWidth + 'px', top: cy + 'px'});
			TweenMax.to(focus, 0.35, {left: cx + 'px', top: cy + 'px'});
			TweenMax.to(end, 0.35, {left: cx + focus.offsetWidth + 'px', top: cy + 'px'});
		}
	};

	function update() {
		curI = 1;
		length = output.length;
		updateContent();
		MESSENGER.listen('reverse/down', rewindStart);
		if(isPlaying) playStart();
	}

	function updateContent() {
		if(output[curI]) {
			start.innerHTML = output[curI].start;
			focus.innerHTML = output[curI].focus;
			end.innerHTML = output[curI].back;
			var perc = curI / length;
			MESSENGER.notify('progressUpdate', perc);
			self.resize(true);
		}
	}

	function playStart() {
		isPlaying = true;
		curI++;
		if(output[curI]) {
			if(output[curI].verse) {
				MESSENGER.notify('updateVerse', {verse:output[curI].verse});
				curI++;
			}
			var delay = setDelay();
			updateContent();
			timer = setTimeout(playStart, delay);
		} else {
			clearTimeout(timer);
			MESSENGER.notify('getNextChapter');
		}
	}

	function rewindStart() {
		isPlaying = true;
		curI--;
		if(output[curI]) {
			if(output[curI].verse) {
				var verse = output[curI].verse - 1;
				MESSENGER.notify('updateVerse', {verse:verse});
				curI--;
			}
			var delay = setDelay();
			updateContent();
			timer = setTimeout(rewindStart, 60);
		} else {
			curI = 1;
			clearTimeout(timer);
			MESSENGER.notify('getPreviousChapter');
			isPlaying = false;
		}
	}

	function stop() {
		isPlaying = false;
		clearTimeout(timer);
	}

	function delayUpdate(e) {
		userDelay = e.value;
	}

	function setDelay() {
		var puncDelay = 0;
		if(output[curI]) {
			if(output[curI].back.indexOf('.') > -1) {
				puncDelay = userDelay * 2;
			} else if(output[curI].back.indexOf('!') > -1) {
				puncDelay = userDelay * 2;
			}  else if(output[curI].back.indexOf('?') > -1) {
				puncDelay = userDelay * 2;
			}  else if(output[curI].back.indexOf(',') > -1) {
				puncDelay = userDelay * 0.75;
			} else if(output[curI].back.indexOf(';') > -1) {
				puncDelay = userDelay * 1.25;
			}
		}
		return userDelay + puncDelay;
	}

	return self;
}
