function Menu() {
	var self = document.createElement('div'),
		content = document.createElement('div'),
		currentChapters;

	self.isOpen = false;

	MESSENGER.listen('resize', menuResize);

	self.setAttribute('class', 'Menu');
	self.style.display = 'none';
	self.style.position = 'absolute';
	self.style.left = 0 + 'px';
	self.style.top = window.innerHeight - 60 + 'px';
	self.style.width = window.innerWidth + 'px';
	self.style.height = window.innerHeight - 60 + 'px';
	self.style.overflowX = 'hidden';
	self.style.overflowY = 'scroll';
	self.style.backgroundColor = '#000000';
  	self.style['-webkit-overflow-scrolling'] = 'touch';

	content.style.display = 'block';
	content.style.position = 'absolute';
	content.style.left = 0 + 'px';
	content.style.top = 0 + 'px';
	content.style.width = window.innerWidth + 'px';
	content.style.height = window.innerHeight - 60 + 'px';

	self.appendChild(content);

	function button(label) {
		var btn = document.createElement('button');

		btn.style.display = 'block';
		btn.style.position = 'absolute';
		btn._width = window.innerWidth;
		btn._height = 30;
		btn.style.width = window.innerWidth + 'px';
		btn.style.height = 30 + 'px';
		btn.style.border = 'none';
		btn.style.borderTop = 1 + 'px solid #222222';
		btn.style.transition = 'all 0.35s';
		btn.style.backgroundColor = 'transparent';
		btn.style.fontFamily = 'Droid Sans Mono';
		btn.style.fontSize = Theme.fontSize.medium;
		btn.style.color = theme.fontLight;
		btn.innerHTML = label;

		if(books[btn.innerHTML] > 1) {
			btn.chapters = chapters(btn.innerHTML);
			btn.chapters.book = btn;
			btn.chapters.style.left = btn.style.left;
			btn.chapters.style.top = parseInt(btn.style.left, 10) + 30 + 'px';
		}

		btn.addEventListener('click', menuButtonClick);
		btn.addEventListener('mouseover', over);

		function menuButtonClick(e) {
			if(btn.chapters) {
				btn.chapters.open(e.target);
			} else {
				new JSONP({url:'php/DataService.php', params:'label=' + btn.innerHTML + '&chapter=' + books[btn.innerHTML], callback:'serviceResponse'});
			}
		}

		return btn;
	}

	function chapters(book) {
		var total = books[book] + 1,
			container = document.createElement('div'),
			i = 1;

		container.style.display = 'block';
		container.style.position = 'absolute';
		container._width = window.innerWidth;
		container._height = 0;
		container.style.width = window.innerWidth + 'px';
		container.style.height = 0 + 'px';
		container.style.overflow = 'hidden';
		container.buttons = document.createElement('div');
		container.buttons.style.display = 'block';
		container.buttons.style.position = 'absolute';

		for(i; i < total; i++) {
			var btn = document.createElement('button');

			btn.style.display = 'block';
			btn.style.position = 'absolute';
			btn.style.width = 44 + 'px';
			btn.style.height = 30 + 'px';
			btn.style.border = 'none';
			btn.style.backgroundColor = 'transparent';
			btn.style.fontFamily = 'Droid Sans Mono';
			btn.style.fontSize = Theme.fontSize.medium;
			btn.style.color = theme.fontLight;
			btn.innerHTML = i;

			container.buttons.appendChild(btn);

			btn.addEventListener('click', chapterClick);
			btn.addEventListener('mouseover', over);
		}

		container.appendChild(container.buttons);

		container.open = function() {
			var prevChapters = currentChapters;
			currentChapters = container;
			chaptersLayout();
			if(prevChapters) {
				prevChapters._height = 0;
				prevChapters.style.height = 0 + 'px';
			}
			TweenMax.to(container, 0.35, {height:container._height});
			menuLayout();
		};

		function chapterClick(e) {
			new JSONP({url:'php/DataService.php', params:'label=' + book + '&chapter=' + e.target.innerHTML, callback:'serviceResponse'});
		}

		return container;

	}

	function buildMenu() {
		for(var key in books) {
			var item = button(key);
			content.appendChild(item);
			if(item.chapters) content.appendChild(item.chapters);
		}
		menuLayout();
	}

	function menuLayout() {
		var length = content.childNodes.length,
			i = 0, nextPos = 0, _w = window.innerWidth;

		for(i; i < length; i++) {
			var child = content.childNodes[i];
			child._width = _w;
			child.style.top = nextPos + 'px';
			child.style.width = _w + 'px';
			nextPos = nextPos + child._height;
		}
		content.style.width = _w + 'px';
		content.style.height = nextPos + 'px';
	}

	function chaptersLayout() {
		if(currentChapters) {
			var topWidth = window.innerWidth,
				btns = currentChapters.buttons,
				length = btns.childNodes.length,
				i = 0,
				nextXPos = 0, nextYPos = 0,
				maxWidth = 0, maxHeight = 0,
				width = window.innerWidth * 0.8,
				row = 0, gap = 0;

			btns.style.width = width + 'px';
			for(i; i < length; i++) {
				var child = btns.children[i],
					cWidth = parseInt(child.style.width, 10),
					cHeight = parseInt(child.style.height, 10);

				if(nextXPos + cWidth < width) {
					child.style.left = nextXPos + 'px';
					child.style.top = nextYPos + 'px';
					gap = 20;
					nextXPos = nextXPos + cWidth + gap;
				} else {
					row++;
					nextXPos = 0;
					nextYPos = row * (cHeight + gap);
					child.style.left = nextXPos + 'px';
					child.style.top = nextYPos + 'px';
					nextXPos = nextXPos + cWidth + gap;
				}
				if(maxWidth < nextXPos) maxWidth = nextXPos;
				if(maxHeight < nextYPos) maxHeight = nextYPos;
			}

			maxWidth -= 20;

			currentChapters._height = maxHeight + 40;
			btns._width = maxWidth;
			btns._height = maxHeight + 40;
			btns.style.width = maxWidth + 'px';
			btns.style.height = maxHeight + 40 + 'px';
			btns.style.left = (topWidth - maxWidth) * 0.5 + 'px';
		}
	}

	self.toggle = function() {
		if(self.isOpen) {
			close();
		} else {
			open();
		}
	}

	function open() {
		self.isOpen = true;
		self.style.display = 'block';
		setTimeout(function() {
			TweenMax.to(self, 0.35, {top:0 + 'px'});
		}, 100);
		MESSENGER.notify('menuOpen');
	}

	function close() {
		self.isOpen = false;
		TweenMax.to(self, 0.35, {top:window.innerHeight - 60 + 'px'});
		setTimeout(function() {
			self.style.display = 'none';
		}, 350);
		MESSENGER.notify('menuClose');
	}

	function over(e) {
		TweenMax.to(e.target, 0.2, {color:theme.highlight});
		e.target.addEventListener(EVENT_OUT, out);
	}

	function out(e) {
		TweenMax.to(e.target, 0.2, {color:theme.fontLight});
		e.target.removeEventListener(EVENT_OUT, out);
	}

	function menuResize() {
		var i = content.children.length;
		while(i--) {
			content.children[i].style.width = window.innerWidth + 'px'
		}
		content.style.width = window.innerWidth + 'px';
		menuLayout();
		chaptersLayout();
		self.style.height = window.innerHeight - 60 + 'px';
		TweenMax.to(self, 0.35, {width:window.innerWidth + 'px'});
	}

	buildMenu();

	return self;

}