function Messenger() {
	
	var messages = {},
		logErrors = false,
		e, errTypes = {
			'1':'FAIL: messenger/ignore - "could not ignore route"',
			'2':'FAIL: messenger/notify - "not listening for route"',
			'3':'WARNING: messenger/notify - "listener count has been exhausted"',
			'4':'FAIL: messenger/notify - "no callback found on route"',
		};

	function listen(route, callback, vars) {
		if(route === 'ERROR') logErrors = true;
		messages[route] = messages[route] ? messages[route] : [];
		if(vars) {
			var runCount = vars.count,
				target = vars.target;
		}
		var r = runCount || 'unlimited',
			t = target || this,
			record = {
				route: route,
				callback: callback,
				target: t,
				runCount: r
			};
		messages[route].push(record);
	}

	function ignore(route, callback, target) {
		if(messages[route]) {
			e = {route:route};
			var len,
				i = messages[route].length,
				t = target || this;
			while(i--) {
				var obj = messages[route][i];
				e.callback = obj.callback.name;
				e.target = obj.target;
				if(isValidIgnore(obj, callback, t)) {
					messages[route].splice(i, 1);
				} else if(logErrors) {
					err('1', e);
				}
			}
			if(messages[route].length < 1) delete messages[route];
		} else if(logErrors) {
			err('2', e);
		}
		if(route === 'ERROR') logErrors = false;
	}

	function ignoreAll(target) {
		for(var key in messages) {
			if(messages.hasOwnProperty(key)) {
				var i = messages[key].length;
				while(i--) {
					var obj = messages[key][i];
					if(obj.target === target) {
						ignore(obj.route, obj.callback, obj.target);
					}
				}
			}
		}
	}

	function notify(route, data, origin) {
		if(messages[route]) {
			e = {route:route};
			var i = messages[route].length;
			while(i--) {
				var obj = messages[route][i];
				e.callback = obj.callback.name;
				e.target = obj.target;
				if(isValidNotify(obj, obj.callback)) {
					obj.callback.apply(obj.target, [data]);
					if(obj.runCount !== 'unlimited') {
						if(obj.runCount === 1) {
							console.log(logErrors);
							if(logErrors) err('3', e);
							ignore(route, obj.callback, obj.target);
						} else {
							obj.runCount--;
						}
					}
				} else if(logErrors) {
					err('4', e);
				}
			}
		} else if(logErrors) {
			err('2', e);
		}
	}

	function isValidIgnore(obj, callback, target) {
		return obj.callback.toString() === callback.toString() && obj.target === target;
	}

	function isValidNotify(obj, callback) {
		return obj.callback.toString() === callback.toString();
	}

	function err(num, e) {
		notify('ERROR', {message:errTypes[num], info:e});
	}

	return {
		messages: messages,
		listen: listen,
		ignore: ignore,
		ignoreAll: ignoreAll,
		notify: notify
	}

}

var MESSENGER = new Messenger();