function Control(type) {
	var self = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
		control = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
		paths = {};

	self.addEventListener(EVENT_DOWN, down);
	self.addEventListener('mouseover', over);

	paths.forward = 'M 0,28 C 0,18.666667 0,9.3333333 0,0 9.333333,4.6666667 18.666667,9.3333333 28,14 18.666667,18.666667 9.333333,23.333333 0,28 z';
	paths.reverse = 'M 0,14 C 9.3333333,9.333333 18.666667,4.666667 28,0 28,9.333333 28,18.666667 28,28 18.666667,23.333333 9.3333333,18.666667 0,14 z';

	control.setAttribute('d', paths[type]);
	control.setAttribute('style', 'fill:' + theme.fontLight + ';');

	self.setAttribute('class', 'Control');
	self.style.display = 'block';
	self.style.position = 'absolute';
	self.style.width = '28px';
	self.style.height = '28px';

	self.appendChild(control);

	function down() {
		self.addEventListener(EVENT_UP, up);
		self.removeEventListener(EVENT_DOWN, down);
		MESSENGER.notify(type + "/down");
	}

	function up() {
		self.removeEventListener(EVENT_UP, up);
		self.addEventListener(EVENT_DOWN, down);
		MESSENGER.notify(type + "/up");
	}

	function over() {
		TweenMax.to(control, 0.35, {fill:theme.highlight});
		self.addEventListener(EVENT_OUT, out);
	}

	function out() {
		TweenMax.to(control, 0.35, {fill:theme.fontLight});
		self.removeEventListener(EVENT_OUT, out);
	}

	return self;
}