var theme = Theme.dark,
	toolbar,textDisplay, menu, slider, info,
	isInit = true,
	output = undefined,
	curI = 1,
	curBook,
	curChapter,
	curVerse,
	view = document.body,
	resizeTimer;

MESSENGER.listen('getNextChapter', getNextChapter);
MESSENGER.listen('getPreviousChapter', getPreviousChapter);

function init() {
	toolbar = new ToolBar();
	textDisplay = new TextDisplay();
	info = new InfoDisplay();
	slider = new SliderControl(0, 1500);
	menu = new Menu();
	theme.fontSize = Theme.fontSize;

	view.style.backgroundColor = theme.overlay;
	view.style.overflow = 'hidden';
	
	view.appendChild(info);
	view.appendChild(slider);
	view.appendChild(menu);
	view.appendChild(toolbar);

	MESSENGER.listen('MenuButton/click', openMenu);

	setTimeout(function() {
		if(!menu.isOpen) menu.toggle();
	}, 1000);
}

function openMenu() {
	menu.toggle();
}

function serviceResponse(e) {
	if(menu.isOpen) menu.toggle();
	curBook = e.book;
	curChapter = e.chapter;
	output = e.output;
	MESSENGER.notify('serviceResponse', {book: e.book, chapter:e.chapter, verse:'1'});
}

function getNextChapter() {
	if(curChapter < books[curBook]) {
		var nextChapter = parseInt(curChapter, 10) + 1;
		new JSONP({url:'php/DataService.php', params:'label=' + curBook + '&chapter=' + nextChapter, callback:'serviceResponse'});
	}
}

function getPreviousChapter() {
	if(curChapter > 1) {
		var prevChapter = parseInt(curChapter, 10) - 1;
		new JSONP({url:'php/DataService.php', params:'label=' + curBook + '&chapter=' + prevChapter, callback:'serviceResponse'});
	}
}

window.addEventListener('load', init);

window.addEventListener('resize', function() {
	clearTimeout(resizeTimer);
	resizeTimer = setTimeout(function() {
		view.style.width = window.innerWidth + 'px';
		toolbar.resize();
		textDisplay.resize();
		MESSENGER.notify('resize');
	}, 200);
});