function InfoDisplay() {
	var self = document.createElement('div'),
		wpm = document.createElement('div'),
		progress = new ProgressBar(),
		timer;

	MESSENGER.listen('sliderUpdate', infoUpdate);

	self.style.display = 'block';
	self.style.position = 'absolute';
	self.style.color = theme.fontLight;
	self.style.backgroundColor = 'transparent';
	self.style.width = window.innerWidth + 'px';
	self.style.height = '20px';
	self.style.top = 2 + 'px';
	self.style.fontSize = Theme.fontSize.small;
	self.style.transition = 'width 0.35s';

	wpm.style.display = 'block';
	wpm.style.position = 'absolute';
	wpm.style.width = 80 + 'px';
	wpm.style.height = '20px';
	wpm.style.left = 10 + 'px';
	wpm.style.top = 0 + 'px';

	self.appendChild(wpm);
	self.appendChild(progress);

	self.style.width = self.offsetWidth + 'px';

	function infoResize() {
		// resize 
	};

	function infoUpdate(e) {
		clearTimeout(timer);
		wpm.style.color = theme.highlight;
		wpm.innerHTML = e.wpm + 'wpm';
		timer = setTimeout(function() {
			TweenMax.to(wpm, 0.35, {color:theme.fontLight});
		},1000);
	}

	return self;
}
