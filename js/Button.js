function Button(label, action) {
	var btn = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
		btnLabelPath = document.createElementNS('http://www.w3.org/2000/svg', 'path'),
		paths = {};
		
	paths.arrow = 'M 10,0 C 13.333333,3.3333333 16.666667,6.6666667 20,10 13.333333,10 6.6666667,10 0,10 3.3333333,6.6666667 6.6666667,3.3333333 10,0 z';
	paths.play = 'M 0,28 C 0,18.666667 0,9.3333333 0,0 9.333333,4.6666667 18.666667,9.3333333 28,14 18.666667,18.666667 9.333333,23.333333 0,28 z';
	paths.rewind = 'M 0,14 C 9.3333333,9.333333 18.666667,4.666667 28,0 28,9.333333 28,18.666667 28,28 18.666667,23.333333 9.3333333,18.666667 0,14 z';

	btnLabelPath.setAttribute('d', paths[label]);
	btnLabelPath.setAttribute('style', 'fill:' + theme.fontLight + ';transition:fill 0.35s;');
	btn.appendChild(btnLabelPath);
	btn.style.width = '28px';
	btn.style.height = '28px';
	btn.style.position = 'absolute';
	btn.style.transition = 'all 0.35s';
	btn.label = label;
	btn.action = action;

	btn.addEventListener('click', action);
	btn.addEventListener('mouseover', over);
	btn.addEventListener('touchstart', over);

	btn.setLabel = function(label) {
		btn.label = label;
		btnLabelPath.setAttribute('d', paths[label]);
		btnLabelPath.setAttribute('style', 'fill:' + theme.fontDark);
	};

	function over() {
		btnLabelPath.style.fill = theme.hoverColor;
		btn.addEventListener('mouseout', out);
		btn.addEventListener('touchend', out);
	}

	function out() {
		btnLabelPath.style.fill = theme.bgDark;
		btn.removeEventListener('mouseout', out);
		btn.removeEventListener('touchend', out);
	}

	btn.resize = function() {
		btn.style.top = window.innerHeight - 60 + 'px';
	};

	return btn;
}
