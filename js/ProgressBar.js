function ProgressBar() {
	var self = document.createElement('div'),
		prog = document.createElement('div'),
		curPerc = 0;

	MESSENGER.listen('resize', progressResize);
	MESSENGER.listen('progressUpdate', updateProgress);
	MESSENGER.listen('serviceResponse', updateProgress);

	self.style.position = 'absolute';
	self.style.display = 'block';
	self.style.left = 80 + 'px';
	self.style.top = 7 + 'px';
	self.style.width = window.innerWidth - 100 + 'px';
	self.style.height = 6 + 'px';
	self.style.backgroundColor = theme.bgDark;
	self.style.borderRadius = 3 + 'px';

	prog.style.position = 'absolute';
	prog.style.display = 'block';
	prog.style.left = 1 + 'px';
	prog.style.top = 1 + 'px';
	prog.style.width = 0 + 'px';
	prog.style.height = 4 + 'px';
	prog.style.backgroundColor = theme.highlight;
	prog.style.borderRadius = 2 + 'px';

	self.appendChild(prog);

	function updateProgress(perc, animate) {
		if(perc.start) {
			curPerc = 0;
		} else {
			curPerc = perc || curPerc;
		}
		var progW = curPerc * ((window.innerWidth - 100) - 1);
		if(animate) {
			TweenMax.to(prog, 0.35, {width:progW + 'px'});
		} else {
			prog.style.width = progW + 'px';
		}
	}

	function progressResize() {
		TweenMax.to(self, 0.35, {width:window.innerWidth - 100 + 'px'});
		updateProgress(curPerc, true);
	}

	return self;
}