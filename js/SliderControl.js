function SliderControl(start, end) {
	var self = document.createElement('div'),
		handle = document.createElement('div'),
		startLabel = document.createElement('div'),
		endLabel = document.createElement('div'),
		_width = Math.round(window.innerWidth * 0.7),
		perc = 500 / end,
		_value = 60000 / perc,
		_wpm;

	MESSENGER.listen('resize', sliderResize);
	MESSENGER.listen('forward/down', sliderHide);
	MESSENGER.listen('forward/up', sliderShow);

	self.style.backgroundColor = theme.bgDark;
	self.style.position = 'absolute';
	self.style.width = _width + 'px';
	self.style.height = '18px';
	self.style.left = (window.innerWidth - _width) * 0.5 + 'px';
	self.style.top = window.innerHeight - 100 + 'px';
	self.style.borderRadius = '9px';
	self.addEventListener('click', clickSetValue);

	handle.style.position = 'absolute';
	handle.style.backgroundColor = theme.highlight;
	handle.style.width = '24px';
	handle.style.height = '16px';
	handle.style.borderRadius = '8px';
	handle.style.left = Math.round(_width * perc) + 'px';
	handle.style.top = 1 + 'px';

	handle.addEventListener(EVENT_DOWN, dragStart);

	startLabel.innerHTML = 'slow';
	startLabel.style.fontSize = Theme.fontSize.small;
	startLabel.style.position = 'absolute';
	startLabel.style.left = -35 + 'px';
	startLabel.style.top = 0 + 'px';
	startLabel.style.color = theme.fontLight;

	endLabel.innerHTML = 'fast';
	endLabel.style.fontSize = Theme.fontSize.small;
	endLabel.style.position = 'absolute';
	endLabel.style.left = _width + 6 + 'px';
	endLabel.style.top = 0 + 'px';
	endLabel.style.color = theme.fontLight;

	self.appendChild(handle);
	self.appendChild(startLabel);
	self.appendChild(endLabel);


	function dragStart(e) {
		self.removeEventListener('click', clickSetValue);
		window.addEventListener(EVENT_MOVE, dragMove);
		window.addEventListener(EVENT_UP, dragStop);
		perc = '';
	}

	function dragMove(e) {
		e.preventDefault();
		var selfBegin = parseInt(self.style.left, 10),
			selfEnd = selfBegin + parseInt(self.style.width, 10),
			eventX = e.targetTouches ? e.targetTouches[0].pageX : e.pageX;
		if(eventX - 12  > selfBegin && eventX + 12 < selfEnd) {
			perc = Math.round((parseInt(handle.style.left, 10) / (_width - 24)) * end);
			handle.style.left = eventX - selfBegin - 12 + 'px';
			_value = Math.round((60000 / perc) / 10) * 10;
			_wpm = Math.round(perc / 10) * 10;
		}
		MESSENGER.notify('sliderUpdate', {value:_value, wpm:_wpm});
	}

	function dragStop(e) {
		self.addEventListener('click', clickSetValue);
		window.removeEventListener(EVENT_MOVE, dragMove);
		window.removeEventListener(EVENT_UP, dragStop);
	}

	function clickSetValue(e) {
		handle.style.transition = 'left 0.35s';
		var selfBegin = parseInt(self.style.left, 10),
			selfEnd = selfBegin + parseInt(self.style.width, 10);
		if(e.pageX - 12  > selfBegin && e.pageX + 12 < selfEnd) {
			handle.style.left = e.pageX - selfBegin - 12 + 'px';
			perc = Math.round((parseInt(handle.style.left, 10) / (_width - 24)) * end);
			_value = Math.round((60000 / perc) / 10) * 10;
			_wpm = Math.round(perc / 10) * 10;
			MESSENGER.notify('sliderUpdate', {value:_value, wpm:_wpm});
		}
		setTimeout(function() {
			handle.style.transition = 'none';
		}, 350);
	}

	function sliderResize() {
		var handlePerc = parseInt(handle.style.left, 10) / _width;
		_width = Math.round(window.innerWidth * 0.7);
		TweenMax.to(self, 0.35, {width:_width + 'px', left:(window.innerWidth - _width) * 0.5 + 'px', top:window.innerHeight - 100 + 'px'});
		TweenMax.to(startLabel, 0.35, {left: -35 + 'px'});
		TweenMax.to(endLabel, 0.35, {left: _width + 6 + 'px'});
		TweenMax.to(handle, 0.35, {left: _width * handlePerc + 'px'});
	}

	function sliderHide() {
		TweenMax.to(self, 0.35, {opacity:0, onComplete:function() {
			self.style.display = 'none';
		}});
	}

	function sliderShow() {
		self.style.display = 'block';
		TweenMax.to(self, 0.35, {opacity:1});
	}

	perc = Math.round((parseInt(handle.style.left, 10) / (_width - 24)) * end);
	_value = Math.round((60000 / perc) / 10) * 10;
	_wpm = Math.round(perc / 10) * 10;

	MESSENGER.notify('sliderUpdate', {value:_value, wpm:_wpm});

	return self;

}
