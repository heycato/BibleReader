var Theme = {
	light:{
		fontDark:'#888',
		fontLight:'#FFF',
		bgDark:'#CCC',
		bgLight:'#FFF',
		hoverColor:'#888'
	},
	dark:{
		fontDark:'#4A4A4A',
		fontLight:'#FFFFFF',
		bgDark:'#202020',
		bgLight:'#4A4A4A',
		highlight:'#74BEFF',
		//highlight:'#FF0000',
		overlay:'#000000',
	},
	fontSize:{
		small:'12px',
		medium:'16px',
		large:'28px'
	}
}

var theme = Theme.dark;
theme.fontSize = Theme.fontSize;
