var JSONP = function JSONP(vars) {

	var jsonp = document.createElement("script"),
		head = document.getElementsByTagName("head")[0];

	jsonp.setAttribute("src", vars.url + "?" + vars.params + '&callback=' + vars.callback);
	jsonp.setAttribute("id", "jsonp");
	var id = document.getElementById("jsonp");

	if(!id) {
		head.appendChild(jsonp);
	} else {
		head.replaceChild(jsonp, id);
	}
};
